package petShop.pet.implementation;

import petShop.food.definition.CatFood;
import petShop.pet.exception.IllegalNameException;
import petShop.pet.exception.SickException;
import petShop.pet.exception.WrongFoodException;

/**
 * Created by marc on 09.03.15.
 */
public class Cat extends APet<CatFood> {

    public Cat(String name) throws IllegalNameException{
        super(name);
    }

    @Override
    public void stroke() {
        System.out.println("Prrrr, Prrr");
    }

    @Override
    public void feed(CatFood f) throws WrongFoodException, SickException {
        if (f == null) {
            super.setSick(true);
            //throw new WrongFoodException();
        }
        if (super.isSick()) {
            throw new SickException();
        }
        System.out.println("Mampf: " + f.getClass().getName());
    }

}
