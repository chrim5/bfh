package petShop.pet.implementation;

import petShop.food.definition.Food;
import petShop.pet.definition.Pet;
import petShop.pet.exception.IllegalNameException;
import petShop.pet.exception.SickException;
import petShop.pet.exception.WrongFoodException;

/**
 * Created by marc on 09.03.15.
 */
public abstract class APet<F extends Food> implements Pet<F> {

    private final String name;
    private boolean isSick;

    public APet(String name) throws IllegalNameException{
        if(name == null) {
            throw new IllegalNameException();
        }
        this.name = name;
    }

    //...methods from Pet must be implemented here

    @Override
    public void stroke() {

    }

    @Override
    public void feed(F food) throws WrongFoodException, SickException {

    }

    protected void setSick(boolean isSick) {
        this.isSick = isSick;
    }

    @Override
    public boolean isSick() {
        return this.isSick;
    }


    @Override
    public void heal() {
        this.isSick = false;
    }

    @Override
    public String getName() {
        return name;
    }
}
