package petShop;

import petShop.food.definition.Food;
import petShop.pet.exception.SickException;
import petShop.pet.exception.WrongFoodException;
import petShop.pet.implementation.Cat;
import petShop.food.implementation.Cheese;
import petShop.pet.definition.Pet;

import javax.swing.*;

/**
 * Created by marc on 09.03.15.
 */
public class Main {

    public static void main(String[] args) throws Exception {
        Pet p = null;
        while (p == null) {
            try {
                String name = JOptionPane.showInputDialog(null, "Name");
                p = new Cat(name);
                System.out.println("Name: " + p.getName());
            } catch (Exception ex) {
                System.out.println("Hey, gib dem Tier einen Namen!");
                main(null);
            }
        }
        boolean hasEaten = false;
        while (!hasEaten) {
            Food f = null;
            int feedCheese = JOptionPane.showConfirmDialog(null, "Shall it get cheese?",
                    "Feeding ceremony", JOptionPane.YES_NO_OPTION);

            if (feedCheese == JOptionPane.YES_OPTION) {
                f = new Cheese();
            }
            try {
                p.feed(f);
                hasEaten = true;
            } catch (WrongFoodException ex) {
                System.out.println("Ou the plate was empty....");
            } catch (SickException ex) {
                System.out.println("Your pet is sick!");
                int healPet = JOptionPane.showConfirmDialog(null, "Do you wanna heal your pet?",
                        "Healing", JOptionPane.YES_NO_OPTION);
                if (healPet == JOptionPane.YES_OPTION) {
                    p.heal();
                    p.stroke();
                } else {
                    System.out.println("Please visit a doctor!!");
                }

            }
        }

    }
}
