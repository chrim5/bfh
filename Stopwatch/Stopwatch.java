package Stopwatch;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * Created by marc on 28.04.15.
 */
public class Stopwatch extends Application{

    @Override
    public void start(Stage stage) throws Exception {
        BorderPane root = new BorderPane();
        root.setPrefSize(300,100);
        HBox hboxCenter = new HBox(5);
        HBox hboxBottom = new HBox(5);
        hboxCenter.setAlignment(Pos.CENTER);
        hboxBottom.setAlignment(Pos.CENTER);
        root.setCenter(hboxCenter);
        root.setBottom(hboxBottom);

        // Create a label and a text field and add them to the scene graph
        Label labelSec = new Label("Sekunden:");
        Label labelTime = new Label("0:00");
        hboxCenter.getChildren().addAll(labelSec, labelTime);

        // Create a button and add it to the scene graph
        Button buttonStart = new Button("Start");
        Button buttonStop = new Button("Stop");
        Button buttonReset = new Button("Reset");
        hboxBottom.getChildren().addAll(buttonStart,buttonStop,buttonReset);

        // Set up the stage
        stage.setTitle("Stopwatch I");
        stage.setScene(new Scene(root, 300, 100));
        stage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
