package Dice;

import javax.xml.bind.annotation.*;
import java.util.Arrays;
import java.util.Random;
@XmlType(name = "Dice", propOrder = {"minValue", "maxValue", "actValue", "statistics"})

/**
 * Created by marc on 07.04.15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(namespace = "http://ch.bfh.xml.dice", name="dice")
public class Dice {
    @XmlElement(name = "minValue", nillable = false, required = true)
    private int minValue;
    @XmlElement(name = "maxValue", nillable = false, required = true)
    private int maxValue;
    @XmlElement(name = "actValue", nillable = false, required = true)
    private int actValue;
    @XmlElement(name = "statistics", nillable = false, required = true)
    private int[] statistics;
    private transient Random random = new Random();

    public Dice(int minValue, int maxValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.statistics = new int[maxValue-minValue+1];
    }

    @SuppressWarnings("unused")
    private Dice() {
        // JAXB need this shizzle
    }

    public int getActValue() {
        return actValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public int getMinValue() {
        return minValue;
    }

    public int[] getStatistics() {
        return statistics.clone();
    }

    public void play() {
        this.actValue = random.nextInt(maxValue - minValue + 1) + minValue;
        this.statistics[actValue-minValue]++;
    }

    @Override
    public String toString() {
        return "Dice{" +
                "minValue=" + minValue +
                ", maxValue=" + maxValue +
                ", actValue=" + actValue +
                ", statistics=" + Arrays.toString(statistics) +
                "}";
    }

}
