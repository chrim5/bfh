package Dice;

import XML.*;
import XML.XMLHelper;
import com.sun.xml.internal.bind.v2.util.ByteArrayOutputStreamEx;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Scanner;

/**
 * Created by marc on 07.04.15.
 */
public class Player {

    public static void main(String[] args) throws MalformedURLException, JAXBException, IOException{
        Dice d = new Dice(1,6);
        for(int i=0; i < 100; i++) {
            d.play();
        }
        System.out.println(d);
        byte[] array;
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        //FileOutputStream os = new FileOutputStream("dice.xml");

        XMLHelper.saveInstance(os, d);
        array=os.toByteArray();
        Scanner s= new Scanner(new ByteArrayInputStream(array));
        while (s.hasNextLine()) {
            System.out.println(s.nextLine());
            //os.write(s.nextLine());
        }
        s.close();
    }
}
