package CompositePattern;

/**
 * Abstract class for employee (component)
 */
public abstract class Employee {
    private String name;
    private int phoneNum;

    // constructor
    public Employee(String name, int phoneNum) {
        this.name = name;
        this.phoneNum = phoneNum;
    }

    // helper function for pretty hierarchy print
    public abstract void print(String pDistance);

    // get number of employees
    public abstract int getEmployeeNumber();

    // getter and setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(int phoneNum) {
        this.phoneNum = phoneNum;
    }
}
