package CompositePattern;

import java.util.ArrayList;
import java.util.List;

/**
 * Composite class department manager
 * which is also an employee
 */
public class DepartmentManager extends Employee {

    private List<Employee> employees = new ArrayList();
    private String department;

    public DepartmentManager(String name, String department, int phoneNum) {
        super(name, phoneNum);
        this.department = department;
    }

    // hint: string parameter is just for spacing
    public void print(String pDistance) {
        System.out.println(pDistance + "Department Manager " + getName() + " (" + getDepartment() + "). Tel: " + getPhoneNum());
        for (Employee e : employees) {
            e.print(pDistance + "      ");//Einrückung
        }
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public int getEmployeeNumber() {
        int sum = 1; //1 for himself
        for (Employee e : employees) {
            sum += e.getEmployeeNumber();
        }
        return sum;
    }

    public void add(Employee emp) {
        employees.add(emp);
    }

    public void remove(Employee emp) {
        employees.remove(emp);
    }

    public Employee getEmployee(int index) {
        return employees.get(index);
    }
}
