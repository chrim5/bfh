package CompositePattern;

/**
 * Leaf class of a simple employee
 */
public class AtomicEmployee extends Employee {

    public AtomicEmployee(String name, int phoneNum) {
        super(name, phoneNum);
    }

    @Override
    public int getEmployeeNumber() {
        return 1;
    }

    //Hinweis: der Stringparameter dient lediglich der Einrückung
    public void print(String pAbstand) {
        System.out.println(pAbstand + getName() + ". Tel: " + getPhoneNum());
    }
}
