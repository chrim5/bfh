package CompositePattern;

/**
 * Created by marc on 02.07.16.
 */
public class TestCompositePattern {

    public static void main(String[] args) {
        DepartmentManager d1 = new DepartmentManager("Werner Fischer", "Vertrieb", 001);
        d1.add(new AtomicEmployee("Peter Meier", 123));
        d1.add(new AtomicEmployee("Paula Schulz", 234));

        DepartmentManager d2 = new DepartmentManager("Hans Küsnacht", "Technologie", 002);
        d2.add(new AtomicEmployee("Hans Werner", 235));
        d2.add(new AtomicEmployee("Franziska Hofbauer", 278));

        DepartmentManager d3 = new DepartmentManager("Fritz Kalkbrenner", "Entwicklung", 003);
        d3.add(new AtomicEmployee("Isabelle Franziskus", 876));

        DepartmentManager executive = new DepartmentManager("Kurt Stauffacher", "Vorstand", 004);
        executive.add(d1);
        executive.add(d2);
        executive.add(d3);
        executive.add(new AtomicEmployee("Vanessa Handori", 748));

        executive.print(" ");

    }
}
