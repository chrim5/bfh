package DecoratorPattern.siders;

import DecoratorPattern.Sider;
import DecoratorPattern.Dish;

/**
 * Created by marc on 01.07.16.
 */
public class FrenchFries extends Sider {

    public FrenchFries(Dish dish) {
        super(dish);
    }

    @Override
    public double getPrice() {
        return dish.getPrice() + 5.0;
    }

    @Override
    public void printDescription() {
        dish.printDescription();
        System.out.print(", Pommes");
    }
}
