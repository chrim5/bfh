package DecoratorPattern.siders;

import DecoratorPattern.Sider;
import DecoratorPattern.Dish;

/**
 * Created by marc on 01.07.16.
 */
public class CeasarSalad extends Sider {
    public CeasarSalad(Dish dish) {
        super(dish);
    }

    @Override
    public double getPrice() {
        return dish.getPrice() + 7.5;
    }

    @Override
    public void printDescription() {
        dish.printDescription();
        System.out.print(", Ceasar Salad");
    }
}
