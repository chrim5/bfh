package DecoratorPattern.test;

import DecoratorPattern.Dish;
import DecoratorPattern.basicDishes.AngusSteak;
import DecoratorPattern.basicDishes.Shrimps;
import DecoratorPattern.siders.CeasarSalad;
import DecoratorPattern.siders.FrenchFries;

/**
 * Created by marc on 01.07.16.
 */
public class Guest {
    public static void main(String[] args) {
        Dish dish = new CeasarSalad(new Shrimps());
        dish.printDescription();
        System.out.println(" für " + dish.getPrice() + " CHF");

        dish = new FrenchFries(new AngusSteak());
        dish.printDescription();
        System.out.println(" für " + dish.getPrice() + " CHF");
    }
}
