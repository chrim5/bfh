package DecoratorPattern.basicDishes;

import DecoratorPattern.Dish;

/**
 * Created by marc on 01.07.16.
 */
public class AngusSteak implements Dish {
    @Override
    public double getPrice() {
        return 25.0;
    }

    @Override
    public void printDescription() {
        System.out.print("Angus Steak");
    }
}
