package DecoratorPattern.basicDishes;

import DecoratorPattern.Dish;

/**
 * Created by marc on 01.07.16.
 */
public class Shrimps implements Dish {
    @Override
    public double getPrice() {
        return 12.0;
    }

    @Override
    public void printDescription() {
        System.out.print("Shrimps");
    }
}
