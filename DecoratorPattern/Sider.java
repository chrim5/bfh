package DecoratorPattern;

/**
 * Created by marc on 01.07.16.
 */
public abstract class Sider implements Dish {
    protected Dish dish;
    public Sider(Dish dish) {
        this.dish = dish;
    }
}
