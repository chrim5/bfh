package DecoratorPattern;

/**
 * Created by marc on 01.07.16.
 */
public interface Dish {
    double getPrice();
    void printDescription();
}
