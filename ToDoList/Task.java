package ToDoList;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by marc on 07.03.16.
 */
public interface Task<T> {

    public boolean add(HashMap T);
    public boolean addValues(String value);
    public boolean delete();
    public Object search();
    public ArrayList sort();
}
