package StopwatchMVC;

import Observer.Time;

/**
 * Created by marc on 11.05.15.
 */
public class Controller {

    private final Time time;
    public Controller(final Time time) {
        this.time = time;
    }
    public void start() {
        // May do some computations, logging, security checks...
        // Control model
        this.time.start();
    }
    public void stop() {
        // May do some computations, logging, security checks...
        // Control model
        this.time.stop();
    }
}
