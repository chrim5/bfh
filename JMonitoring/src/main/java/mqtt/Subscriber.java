package mqtt;

import model.MemObject;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.MqttException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

/**
 * Created by marc.christen on 18/06/15.
 */
public class Subscriber extends Observable {

    MqttClient client;
    public HashMap<String, Integer> memData;
    private String message;
    private ArrayList<MemObject> memObject;

    public ArrayList<MemObject> getMemInfo() {
        return memObject;
    }
    public MemObject getLatestInfo() {
        return memObject.get(memObject.size()-1);
    }

    public Subscriber() {
        memData = new HashMap<>();
        memObject = new ArrayList<MemObject>();

        try {
            client = new MqttClient("tcp://localhost:1883", "Subscriber");
            client.connect();
            client.setCallback(new MqttCallback() {

                @Override
                public void connectionLost(Throwable throwable) {
                    System.out.println("Connection lost...");
                }

                @Override
                public void messageArrived(String topic, MqttMessage mqttMessage)
                        throws Exception {
                    message = new String(mqttMessage.getPayload());
                    MemObject m = new MemObject(message);
                    memObject.add(m);

                    setChanged();
                    notifyObservers(memObject);

                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

                }

                });
            client.subscribe("JMonitor");
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

}