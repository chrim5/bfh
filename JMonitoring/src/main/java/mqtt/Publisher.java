package mqtt;

import model.MemUsage;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.IOException;

/**
 * Created by marc.christen on 18/06/15.
 */
public class Publisher implements Runnable{

    private volatile boolean running;

    public void terminate() {
        running = false;
    }

    MqttClient client;

    public Publisher() {
        this.running = true;
    }

    public void run() {
        while (running) {
            try { Thread.sleep(1000);
                client = new MqttClient("tcp://localhost:1883", "JMonitorPublish");
                client.connect();
                MemUsage mem1 = new MemUsage();
                MqttMessage message = new MqttMessage();
                message.setPayload(String.valueOf(mem1.getMemInfo()).getBytes());
                client.publish("JMonitor", message);
                client.disconnect();
            } catch (InterruptedException e) {} catch (IOException e) {
                e.printStackTrace();
                running = false;
            } catch (MqttPersistenceException e) {
                e.printStackTrace();
            } catch (MqttSecurityException e) {
                e.printStackTrace();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }
}