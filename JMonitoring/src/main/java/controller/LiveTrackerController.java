package controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import model.MemObject;
import mqtt.Publisher;
import mqtt.Subscriber;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.IOException;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

/**
 * Created by marc.christen on 16/06/15.
 */
public class LiveTrackerController implements Initializable,Observer {

    // Publisher thread
    private Thread thread = null;
    private Publisher runnable = null;
    // Subscriber aka model
    private Subscriber data;
    private long startTime;
    @FXML private PieChart swapChart;
    @FXML private Button start;
    @FXML private Button stop;
    private ObservableList<PieChart.Data> swapChartData;
    private final XYChart.Series series1 = new XYChart.Series<>();
    private final XYChart.Series series2 = new XYChart.Series<>();


    @FXML public LineChart<Number,Number> memChart;
    //@FXML public NumberAxis yAxis;
    @FXML private Text actiontarget;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        data = new Subscriber();
        startTime = 0;

        // Memory chart
        memChart.setCreateSymbols(false);
        memChart.getData().add(new XYChart.Series<>());
        memChart.getData().addAll(series1,series2);

        // Swap chart
        swapChartData = FXCollections.observableArrayList();
        swapChart.setData(swapChartData);
        swapChartData.add(new PieChart.Data("SwapUsed", 0.0));
        swapChartData.add(new PieChart.Data("SwapFree", 0.0));

        // ready for updates!
        data.addObserver(this);
    }
    @FXML protected void clear(ActionEvent event){
        swapChartData.clear();
        series1.getData().clear();
        series2.getData().clear();
    }

    @FXML protected void startThread(ActionEvent event)
            throws IOException, InterruptedException, MqttException {
        //init();
        runnable = new Publisher();
        // Start new thread and put it in the background!
        // If you run it in the UI thread, it will become not responsible
        thread = new Thread(runnable);
        thread.start();
        actiontarget.setText("Background process successfully started.");
        start.setDisable(true);
    }

    @FXML protected void stopThread(ActionEvent event)
            throws InterruptedException {

        if (thread != null) {
            runnable.terminate();
            thread.join();
            actiontarget.setText("Thread successfully stopped.");
            start.setDisable(false);
        }
    }


    @Override
    public void update(Observable o, Object arg) {

        MemObject p = data.getLatestInfo();
        // Update UI thread

/*        HashMap.Entry<String, Integer> entry = (HashMap.Entry) arg;
        boolean isNew = true;
        for (PieChart.Data d : swapChartData) {
            if (d.getName().equals(entry.getKey()) ) {
                Platform.runLater(()->d.setPieValue(entry.getValue()));
                isNew = false;
                break; }
        }
        if (isNew) {
            Platform.runLater(()->this.swapChartData.add(
                    new PieChart.Data(entry.getKey(), entry.getValue())));
        }*/
        Platform.runLater(() -> {
            //if first point, start timer
            if (startTime == 0)
                startTime = System.currentTimeMillis() / 1000l;

            //calculating seconds since start of tracking
            long seconds = (System.currentTimeMillis() / 1000l) - startTime;



            //adding data to memory chart
            // Debug
            //System.out.println(p.getUsedMemory());

            // Series aka lines for memory chart
            series1.setName("Memory in use");
            series2.setName("Memory max");
            series1.getData().add(new XYChart.Data(seconds, p.getUsedMemory()));
            series2.getData().add(new XYChart.Data(seconds, p.getFreeMemory()+p.getUsedMemory()));

            // Swap chart
            //System.out.println(p.getUsedSwap());
            //swapChartData.add(new PieChart.Data("SwapUsed", p.getUsedSwap()));
            //swapChartData.add(new PieChart.Data("SwapFree", p.getFreeSwap()));
            swapChartData.get(0).setPieValue(p.getUsedSwap());
            swapChartData.get(1).setPieValue(p.getFreeSwap());

       });

    }
}
