package model;

import org.json.*;

/**
 * Created by marc.christen on 22/06/15.
 */
public class MemObject {
    private int usedMemory;
    private int freeMemory;
    private double usedSwap;
    private double freeSwap;

    public int getUsedMemory() {
        return usedMemory;
    }

    public void setUsedMemory(int usedMemory) {
        this.usedMemory = usedMemory;
    }

    public int getFreeMemory() {
        return freeMemory;
    }

    public void setFreeMemory(int freeMemory) {
        this.freeMemory = freeMemory;
    }

    public double getUsedSwap() {
        return usedSwap;
    }

    public void setUsedSwap(double usedSwap) {
        this.usedSwap = usedSwap;
    }

    public double getFreeSwap() {
        return freeSwap;
    }

    public void setFreeSwap(double freeSwap) {
        this.freeSwap = freeSwap;
    }

    public MemObject(String jsonData)
    {
        try
        {
            JSONObject obj = new JSONObject(jsonData);
            usedMemory = obj.getInt("UsedMemory");
            freeMemory = obj.getInt("FreeMemory");
            usedSwap = obj.getDouble("UsedSwap");
            freeSwap = obj.getDouble("FreeSwap");

        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public String toString()
    {
        return "MemObject{" +
                "UsedMemory=" + usedMemory +
                ", FreeMemory=" + freeMemory +
                ", UsedSwap=" + usedSwap +
                ", FreeSwap=" + freeSwap +
                '}';
    }
}
