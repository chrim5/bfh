package model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by marc.christen on 14/06/15.
 */
public class MemUsage {

    private long physMemory;
    private long pagesFree;
    private long memUsed;
    private int result;
    private ArrayList<Double> swapUsage;


    public MemUsage() throws IOException, InterruptedException {
        this.swapUsage = new ArrayList<Double>(3);
    }

    private long calcByteToMegaByte(long value) {
        this.result = (int) (value/1048576);
        return this.result;
    }

    public String getMemInfo() throws IOException, InterruptedException {
        this.getPhysicalMemory();
        this.getMemoryUsed();
        this.getSwapUsage();

        JSONObject obj = new JSONObject();
        try {

            obj.put("UsedSwap", this.swapUsage.get(1));
            obj.put("FreeSwap", this.swapUsage.get(2));
            obj.put("UsedMemory", calcByteToMegaByte(this.memUsed));
            obj.put("FreeMemory", calcByteToMegaByte(this.physMemory - this.memUsed));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }

    public long getPhysicalMemory() throws IOException, InterruptedException {
        Runtime r = Runtime.getRuntime();

        //String cmd = "sysctl hw.memsize | awk '{print $2}'";
        String cmd = "sysctl hw.memsize";
        Process p = r.exec(cmd);
        p.waitFor();

        InputStream is = p.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line = null;
        //BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
        //String line = "";

        while ((line = br.readLine()) != null) {
            //System.out.println(line);
            String value = String.valueOf(line.split(": ")[1]);
            this.physMemory = Long.parseLong(value);
        }

        br.close();
        return this.physMemory;

    }

    public long getMemoryUsed() throws IOException, InterruptedException {
        Runtime r = Runtime.getRuntime();

        //String cmd = "vm_stat | grep \"Pages free\" | awk {'print $3'}";

        String[] cmd = {
                "/bin/sh",
                "-c",
                "vm_stat | grep \"Pages free\" | awk {'print $3'} | sed 's/.$//'"
        };
        Process p = r.exec(cmd);
        p.waitFor();
        BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line = null;

        while ((line = b.readLine()) != null) {
            //System.out.println(line);
            //String value = String.valueOf(line.split())
            this.pagesFree = Long.parseLong(line);
            //System.out.println(pagesFree);
        }

        b.close();

        this.memUsed = this.physMemory - (this.pagesFree * 4096);
        return this.memUsed;

    }

    public ArrayList<Double> getSwapUsage() throws IOException, InterruptedException {
        Runtime r = Runtime.getRuntime();

        final String[] swapTotal = {
                "/bin/sh",
                "-c",
                "sysctl vm.swapusage | awk {'print $4'} | sed 's/.$//'"
        };

        final String[] swapUsed = {
                "/bin/sh",
                "-c",
                "sysctl vm.swapusage | awk {'print $7'} | sed 's/.$//'"
        };

        final String[] swapFree = {
                "/bin/sh",
                "-c",
                "sysctl vm.swapusage | awk {'print $10'} | sed 's/.$//'"
        };

        ArrayList<String[]> cmdList = new ArrayList<String[]>() {{
            add(swapTotal);
            add(swapUsed);
            add(swapFree);
        }};

        Iterator<String[]> cmdListIterator = cmdList.iterator();
        while (cmdListIterator.hasNext()) {

            Process p = r.exec(cmdListIterator.next());
            p.waitFor();
            BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;

            while ((line = b.readLine()) != null) {
                this.swapUsage.add(Double.parseDouble(line));
            }
            b.close();
        }
        return this.swapUsage;

    }

}
