package model.sensor.definition;

/**
 * Created by marc.christen on 04/06/15.
 */
public class DiskSensor implements Sensor {

    public boolean isRunning() {
        return false;
    }

    public void start() {

    }

    public void stop() {

    }
}
