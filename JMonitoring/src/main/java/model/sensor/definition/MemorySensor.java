package model.sensor.definition;


/**
 * Created by marc.christen on 04/06/15.
 */
public class MemorySensor implements Sensor {
    private boolean run;

    public MemorySensor(boolean run) {
        this.run = run;
    }

    public boolean isRunning() {
        return run;
    }

    public void start() {
        run = true;
    }

    public void stop() {
        run = false;
    }

}
