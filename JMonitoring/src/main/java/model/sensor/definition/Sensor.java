package model.sensor.definition;

/**
 * Created by marc.christen on 04/06/15.
 */
public interface Sensor {

    public boolean isRunning();
    public void start();
    public void stop();

}
