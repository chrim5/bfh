package model;

import model.sensor.definition.CPUSensor;
import com.sun.management.OperatingSystemMXBean;

import java.lang.management.ManagementFactory;


/**
 * Created by marc.christen on 04/06/15.
 */
public class CPUUsage extends CPUSensor {

    public CPUUsage(boolean run) {
        super();
        this.getCPUInfo();

    }

    public void getCPUInfo() {
        OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(
                OperatingSystemMXBean.class);
        System.out.println("System load: " + osBean.getSystemLoadAverage());
    }


}
