import controller.LiveTrackerController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by marc.christen on 04/06/15.
 */
public class Main extends Application{

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(
                getClass().getClassLoader().getResource("fxml/LiveTracker.fxml"));
        Parent root = loader.load();
        //Subscriber memData = new Subscriber();
        //loader.<LiveTrackerController>getController();

        Scene scene = new Scene(root, 1090, 800);

        stage.setTitle("JMonitoring");
        stage.setScene(scene);
        stage.show();

        stage.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });
    }

    public static void main(String[] args) {
        Application.launch(Main.class, args);
    }
}
