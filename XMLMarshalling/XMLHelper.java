package XMLMarshalling;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by marc on 07.04.15.
 */
public class XMLHelper {

    public static void saveInstance (OutputStream outputStream , Object instance)
            throws JAXBException, IOException {
        Marshaller marshaller = JAXBContext.newInstance(instance.getClass()).createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(instance, outputStream);
        outputStream.flush();

    }


}
