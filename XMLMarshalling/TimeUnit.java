package XMLMarshalling;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by marc on 07.04.15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(namespace = "http://ch.fbi.xml.beispielFuenf", name = "time_unit")
public class TimeUnit {
    private String UNIT_LABEL;

    public TimeUnit(String unitLabel){
        this.UNIT_LABEL=unitLabel;
    }
    @SuppressWarnings("unused")
    private TimeUnit(){
        //only for JAXB -> Magic!
    }
    /*
    public void setUnitLabel(String uLabel) {
        this.UNIT_LABEL = uLabel;
    }
    */
    public String getUnitLabel() {
        return UNIT_LABEL ;
    }
}
