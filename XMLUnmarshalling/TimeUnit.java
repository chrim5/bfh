package XMLUnmarshalling;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by marc on 13.04.15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(namespace = "http://ch.fbi.xml.beispielSechs", name = "time_unit")
public class TimeUnit {
    @XmlElement(name = "unit_label")
    private String UNIT_LABEL;

    @SuppressWarnings("unused")
    private TimeUnit() {
        //only for JAXB -> Magic!
    }

    public String getUnitLabel() {
        return UNIT_LABEL;
    }

}