package XMLUnmarshalling;

import javax.xml.bind.*;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

/**
 * Created by marc on 13.04.15.
 */
@SuppressWarnings("rawtypes")
public class XMLHelper {

    static class LocalFileSchemaResolver extends SchemaOutputResolver {
        private File baseDir;

        public LocalFileSchemaResolver(File baseDir) {
            this.baseDir = baseDir;
        }

        public Result createOutput(String namespaceUri, String suggestedFileName)
                throws IOException {
            return new StreamResult(new File(baseDir, suggestedFileName));
        }
    }

    public static void saveSchema(File baseDir, Class... classes)
            throws JAXBException , IOException {
        JAXBContext context = JAXBContext.newInstance(classes) ;
        context.generateSchema(new LocalFileSchemaResolver(baseDir));
    }


    public static Object loadInstance(InputStream inputStream,Class instanceClass)
            throws JAXBException {
                Unmarshaller unmarshaller = JAXBContext.newInstance((instanceClass)).createUnmarshaller();
                Object instance = unmarshaller.unmarshal(inputStream);
                return instance;
    }

    public static void saveInstance (OutputStream outputStream , Object instance)
            throws JAXBException, IOException {
        Marshaller marshaller = JAXBContext.newInstance(instance.getClass()).createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(instance, outputStream);
        outputStream.flush();

    }
    public static void saveInstance (OutputStream outputStream , URL schemaURL, String schemaName, Object instance)
            throws JAXBException, IOException {
        Marshaller marshaller = JAXBContext.newInstance(instance.getClass()).createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION , schemaURL + " " + schemaName);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(instance, outputStream);
        outputStream.flush();

    }
}