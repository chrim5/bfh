package XMLUnmarshalling;

import Dice.Dice;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by marc on 07.04.15.
 */
public class XMLTester {

    public static void main(String[] args) throws MalformedURLException, JAXBException, IOException {
        // Play with dice!
        Dice dice = new Dice(1,6);
        for(int i=0; i < 100; i++) {
            dice.play();
        }
        byte [] array ;
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            // XML Schema
            XMLHelper.saveSchema(new File("."), Dice.class);
            //XMLHelper.saveInstance(os, dice);
            XMLHelper.saveInstance(os, new URL("http://ch.bfh.xml.dice"), "dice", dice);

            os.close();
            array = os.toByteArray();

            InputStream is = new ByteArrayInputStream(array);

            DiceStatistics statistics = (DiceStatistics)XMLHelper.loadInstance(is, DiceStatistics.class);

            System.out.println(statistics);
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }
}
