package XMLUnmarshalling;

import javax.xml.bind.annotation.*;
import java.util.Arrays;

/**
 * Created by marc on 13.04.15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Dice", propOrder = {"statistics"})
@XmlRootElement(namespace = "http://ch.bfh.xml.dice", name = "dice")
public class DiceStatistics {

        @XmlElement(name="statistics", nillable = false, required = true)
        private int[] statistics;

    private DiceStatistics(){
            //JAXB
        }

        public int[] getStatistics() {
            return statistics.clone();
        }


        @Override
        public String toString() {
            return "DiceStatistics [statistics =" + Arrays.toString(statistics) + "]";
        }
}
