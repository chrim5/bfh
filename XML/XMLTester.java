package XML;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Scanner;

/**
 * Created by marc on 07.04.15.
 */
public class XMLTester {

    public static void main(String[] args) {
        byte [] array ;
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            TimeUnit unit1 = new TimeUnit();
            XMLHelper.saveInstance(os, unit1);
            os.close() ;
            array = os.toByteArray();

            Scanner s = new Scanner(new ByteArrayInputStream(array));
            while (s.hasNextLine())
                System.out.println(s.nextLine()) ;
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }
}
