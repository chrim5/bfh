package Threads;

/**
 * Created by marc on 10.03.15.
 */
public class Program {

    public static void main(String[] args) {
        Thread t1 = new Thread(new CharPrinter('+', 5000));
        Thread t2 = new Thread(new CharPrinter('/', 3500));

        t1.setName("Thread one");
        t2.setName("Thread two");
        System.out.println(" "); System.out.println("before start");
        t1.start();
        t2.start();
        System.out.println(" "); System.out.println("threads started");
        try {
            t1.join(); //blocks until thread_1 is terminated
            t2.join(); //blocks until thread_1 is terminated
        } catch (InterruptedException e) {
            // main thread has been interrupted
            e.printStackTrace();
        }
        System.out.println(" "); System.out.println("threads finished");
    }
}
