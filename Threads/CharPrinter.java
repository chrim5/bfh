package Threads;

import java.util.Random;
import static java.lang.Thread.sleep;

/**
 * Created by marc on 10.03.15.
 */
public class CharPrinter implements Runnable {

    private static Random randomGen;

    static {
        randomGen = new Random();
    }

    // dynamic-object declaration
    private char showChar;
    private int delayRange;

    public CharPrinter(char inChar, int inDelayRange){
        this.showChar = inChar;
        this.delayRange = inDelayRange;
    }

    @Override
    public void run() {
        int delay;
        for (int i=0; i < 5; i++) {
            System.out.print(showChar);
            delay = randomGen.nextInt(this.delayRange);
            try {
                sleep(delay);
            } catch (Exception e) {
                System.out.println("I, i.e " + Thread.currentThread().getName() + ", have been interrupted");
                // nothing shall happen in case of interrupt
                return;
            }
        }

    }

}
