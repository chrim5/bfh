package Observer;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import java.util.Observable;

/**
 * Created by marc on 11.05.15.
 */
public class DigitalClock extends Stage implements Observer {
    private final Label timeLabel = null;
    private final Time time;

    public DigitalClock(final Time time) {
        this.time = time;
        this.time.addObserver(this);
        //...
    }

    @Override
    public void update(Observable o, Object arg) {
        // Make sure, the GUI is updated in the JavaFX Application Thread!
        Platform.runLater(() -> {
            int s = this.time.getTime();
            this.timeLabel.setText(Integer.toString(s));
        });
    }

}