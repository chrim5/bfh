package Observer;

import StopwatchMVC.Controller;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Created by marc on 11.05.15.
 */
public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Time time = new Time();
        Controller controller = new Controller(time);
        //new AnalogClock(time);
        new DigitalClock(time, controller);
        //new ControllerGUI(time, controller);
        //new ControllerConsole(controller);
    }
    public static void main(String[] args) {
        launch(args);
    }
}