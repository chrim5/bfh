package Observer;

import java.util.Observable;

/**
 * Created by marc on 11.05.15.
 */

public class Time extends Observable implements Runnable {
    private int time;
    public Time() {
        this.time =
                (int)(System.currentTimeMillis()/1000) % (60*60*24);
        // Start clock
        (new Thread(this)).start();
    }
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
                this.increaseTime();
            } catch (InterruptedException e) {}
        }
    }

    private void increaseTime() {
        this.time += 1;
        // Important:
        // Call setChanged() before calling notifyObservers()
        this.setChanged();
        this.notifyObservers();
    }
    public int getTime() {
        return this.time;
    }

    public void addObserver(DigitalClock digitalClock) {
    }

    public void start() {
    }
}