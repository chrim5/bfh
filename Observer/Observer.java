package Observer;

import java.util.Observable;

/**
 * Created by marc on 11.05.15.
 */
public interface Observer {

    public void update(Observable o, Object arg);
}
